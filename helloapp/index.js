import express from 'express';
const app = express();

const appID = `My Random ID is ${Math.random()}`;
let counter = 0;

app.get('/', (req, res) => {
  res.send(`Hello from ${appID}! you are request number ${++counter} I am a healthy!!!`);
  console.log(`I am request number ${counter}`);
})

app.get('/hello', (req, res) => {
  res.send(`Hello from ${appID}! you are request number ${++counter}`);
})

app.get('/shutdown', (req, res)=> {
  res.send(`Shutting down... app ID:${appID}`);
  console.log(`Shutting down... app ID:${appID}`);
  process.exit(0);
})

app.listen(3000, () => console.log(`Started app ${appID} `))