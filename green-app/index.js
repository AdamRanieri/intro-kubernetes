const express = require('express');
const app = express();

app.get('/getpass', (req,res)=>{

    const envVar = process.env.PASSWORD
    res.send('the environment variable is ' + envVar)

})


app.get('/getusername', (req,res)=>{

    const envVar = process.env.USERNAME
    res.send('the environment variable is ' + envVar)

})

app.listen(3000,()=>console.log('App started!!!'))