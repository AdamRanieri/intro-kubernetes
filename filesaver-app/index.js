const express = require('express');
const app = express();
const {readFile, writeFile} = require('fs').promises;

app.get('/addperson/:name', async (req,res)=>{
    const fileData = await readFile('./logs/persons.txt');
    const info = fileData.toString();
    const updatedInfo = info + req.params.name;
    await writeFile('./logs/persons.txt', updatedInfo);
    res.send('saved name to the file')
})

app.get('/people', async (req,res)=>{
    const fileData = await readFile('./logs/persons.txt')
    const info = fileData.toString()
    res.send(info)
})

app.get('/reset', async (req,res)=>{
    await writeFile('./logs/persons.txt', '');
    res.send('saved name to the file')
})

app.listen(3000, ()=>console.log('App Started'))